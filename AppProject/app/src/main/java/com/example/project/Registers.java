package com.example.project;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Registers {

    public static String BASE_URL = "https://itlearningcenters.com/android/project0810/";
    @FormUrlEncoded
    @POST("regist_cust.php")
    public Call<ResponseBody> registers   (@Field("cust_id") String cusIdValue,
                                           @Field("cust_name") String cusNameValue,
                                           @Field("email") String cusEmailValue,
                                           @Field("password") String cusPasswordValue,
                                           @Field("address") String cusAddressValue,
                                           @Field("phone") String cusPhoneValue);


    public static String LogBASE_URL = "https://itlearningcenters.com/android/project0810/";
    @FormUrlEncoded
    @POST("cust_login.php")
    Call<ResponseBody> login( @Field("cust_name") String cusNameValue,
                              @Field("password") String cusPasswordValue);

}
