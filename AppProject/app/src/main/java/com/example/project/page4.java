package com.example.project;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class page4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page4);

        ImageView imageView1;
        imageView1 = (ImageView) findViewById(R.id.imageView19);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page4.class);
                startActivity(i);
            }
        });

        ImageView imageView2;
        imageView2 = (ImageView) findViewById(R.id.imageView18);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page5.class);
                startActivity(i);
            }
        });

        ImageView imageView3;
        imageView3 = (ImageView) findViewById(R.id.imageView10);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page7.class);
                startActivity(i);
            }
        });

        ImageView imageView4;
        imageView4 = (ImageView) findViewById(R.id.imageView21);
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page9.class);
                startActivity(i);
            }
        });

        ImageView imageView5;
        imageView5 = (ImageView) findViewById(R.id.imageView13);
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page10.class);
                startActivity(i);
            }
        });

        ImageView imageView6;
        imageView6 = (ImageView) findViewById(R.id.imageView12);
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page11.class);
                startActivity(i);
            }
        });

        ImageView imageView7;
        imageView7 = (ImageView) findViewById(R.id.imageView17);
        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, MainActivity.class);
                startActivity(i);
            }
        });

        ImageView imageView8;
        imageView8 = (ImageView) findViewById(R.id.imageView9);
        imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page8.class);
                startActivity(i);
            }
        });

        ImageView imageView9;
        imageView9 = (ImageView) findViewById(R.id.imageView8);
        imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page81.class);
                startActivity(i);
            }
        });

        ImageView imageView10;
        imageView10 = (ImageView) findViewById(R.id.imageView36);
        imageView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page4.this, page10.class);
                startActivity(i);
            }
        });
    }
}
