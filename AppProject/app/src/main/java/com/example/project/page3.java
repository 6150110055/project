package com.example.project;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class page3 extends AppCompatActivity {

    private static String TAG_RETROFIT_GET_POST = "RETROFIT_GET_POST";
    private ProgressDialog progressDialog = null;
    Button btn1;
    EditText cusNameEditText,cusPasswordEditText ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page3);

        initControls();

        ImageView imageView1;
        imageView1 = (ImageView) findViewById(R.id.imageView5);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page3.this, MainActivity.class);
                startActivity(i);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog();

                cusNameEditText = findViewById(R.id.editTextTextPersonName4);
                cusPasswordEditText = findViewById(R.id.editTextTextPassword);
                final String cusNameValue = cusNameEditText.getText().toString();
                final String cusPasswordValue = cusPasswordEditText.getText().toString();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Registers.BASE_URL).addConverterFactory(GsonConverterFactory.create())
                        .build();
                //Create instance for user manager interface and return it.
                Registers registerInterface = retrofit.create(Registers.class);
                // Use default converter factory, so parse response body text took http3.ResponseBody object.
                Call<ResponseBody> call = registerInterface.login(cusNameValue,cusPasswordValue);
                // Create a Callback object, because we do not set JSON converter, so the return object is ResponseBody be default.
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        hideProgressDialog();
                        StringBuffer messageBuffer = new StringBuffer();
                        int statusCode = response.code();
                        if(statusCode == 200)
                        {
                            try {
                                // Get return string.
                                String returnBodyText = response.body().string();
                                // Because return text is a json format string, so we should parse it manually.
                                Gson gson = new Gson();
                                TypeToken<List<RegisterResponse>> typeToken = new
                                        TypeToken<List<RegisterResponse>>(){};
                                // Get the response data list from JSON string.
                                List<RegisterResponse> registerResponseList =
                                        gson.fromJson(returnBodyText, typeToken.getType());
                                if(registerResponseList!=null &&
                                        registerResponseList.size() > 0) {
                                    RegisterResponse registerResponse =
                                            registerResponseList.get(0);
                                    if (registerResponse.isSuccess()) {

                                        messageBuffer.append("Login Success");
                                        Intent i = new Intent(page3.this, page4.class);
                                        startActivity(i);

                                    } else {
                                        messageBuffer.append("User login failed.");
                                    }
                                }
                            }catch(IOException ex)
                            {
                                Log.e(TAG_RETROFIT_GET_POST, ex.getMessage());
                            }
                        }else
                        {
                            // If server return error.
                            messageBuffer.append("Server return error code is ");
                            messageBuffer.append(statusCode);
                            messageBuffer.append("\r\n\r\n");
                            messageBuffer.append("Error message is ");
                            messageBuffer.append(response.message());
                        }
                        // Show a Toast message.
                        Toast.makeText(getApplicationContext(),
                                messageBuffer.toString(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), t.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
    /* Initialize all UI controls. */
    private void initControls()
    {

        if(cusNameEditText==null)
        {
            cusNameEditText = (EditText)findViewById(R.id.custname);
        }

        if(cusPasswordEditText==null)
        {
            cusPasswordEditText = (EditText)findViewById(R.id.password);
        }

        if(btn1 == null)
        {
            btn1 = (Button)findViewById(R.id.button4);
        }
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(page3.this);
        }
    }
    /* Show progress dialog. */
    private void showProgressDialog()
    {
        // Set progress dialog display message.
        progressDialog.setMessage("Please Wait");
        // The progress dialog can not be cancelled.
        progressDialog.setCancelable(false);
        // Show it.
        progressDialog.show();
    }
    /* Hide progress dialog. */
    private void hideProgressDialog()
    {
        // Close it.
        progressDialog.hide();
    }

}
